package gt.com.hermes.cinekinal;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Hermes on 18/07/2015.
 */
public interface PeliculaService {
    @GET("/peliculas")
    void getPelicula(Callback<List<Pelicula>> callback);
}
