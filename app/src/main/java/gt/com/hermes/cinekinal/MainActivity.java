package gt.com.hermes.cinekinal;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {

    private ListView lvPeliculas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvPeliculas = (ListView) findViewById(R.id.lvPeliculas);

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://192.168.0.26/CineKinal/public").build();

        PeliculaService service = restAdapter.create(PeliculaService.class);
        service.getPelicula(new Callback<List<Pelicula>>() {
            @Override
            public void success(List<Pelicula> peliculas, Response response) {
                AdaptadorPeliculas adapter = new AdaptadorPeliculas(MainActivity.this, peliculas);
                lvPeliculas.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AdaptadorPeliculas extends ArrayAdapter<Pelicula>{
        private List<Pelicula> listaPeliculas;

        public AdaptadorPeliculas(Context context, List<Pelicula> pelis){
            super(context, R.layout.listitem_peliculas, pelis);
            listaPeliculas = pelis;
        }
        public View getView(int position, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.listitem_peliculas, null);

            TextView txtTitulo = (TextView)item.findViewById(R.id.tvTitulo);
            txtTitulo.setText(listaPeliculas.get(position).getTitulo());

            TextView txtDescripcion = (TextView)item.findViewById(R.id.tvDescripcion);
            txtDescripcion.setText(listaPeliculas.get(position).getDescripcion());

            return item;
        }
    }
}

